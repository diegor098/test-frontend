import React from 'react';
import './App.css';
import SearchBox from './components/search-box/SearchBox';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import SearchResult from './components/search-result/SearchResult';
import ProductDetail from './components/product-detail/ProductDetail';

function App() {
  return (
    <Router>
      <div>
        <SearchBox />
        <Switch>
          <Route path="/items/detail/:id">
            <ProductDetail />
          </Route>
          <Route path="/items/:search">
            <SearchResult />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
