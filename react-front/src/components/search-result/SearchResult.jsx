import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import {
    Link
} from "react-router-dom";
import envio from '../../assets/images/envio.png';


const SearchResult = () => {

    const { search } = useParams();

    const [products, setProducts] = useState([]);

    React.useEffect(() => {
        const getSearchResult = async () => {
            const data = await fetch(`http://localhost:3001/api/items?q=${search}`);
            const productsData = await data.json();
            setProducts(productsData.result);
        }
        getSearchResult()
    }, [search]);




    return (
        <div className="container">
            <div className="col-sm-12">
                {/*Se deja estático el breadcrumb porque en la documentación del test no se especifica con que propiedad tomarlo o armarlo*/}<p style={{ marginTop: '16px', marginBottom: '16px' }}>Telefonos {'>'} Celulares {'>'} Celulares</p>
                <ul className="list-group mt-4">
                    {
                        products.items && products.items.map((product, i) => (
                            i < 4 ?
                                <Link to={`/items/detail/${product.id}`} key={product.id}>
                                    <li className="list-group-item" style={{ height: '225px' }}>
                                        <span className="pull-left ">
                                            <img style={{ marginTop: '16px', marginBottom: '16px', borderRadius: '4px', marginRight: '16px', marginLeft: '16px' }} src={product.picture} alt="cargando" width="180" height="180" />
                                        </span>
                                        <div>
                                            <p style={{ marginBottom: '32px', fontSize: 'large' }}><NumberFormat value={product.price.amount} displayType={'text'} thousandSeparator={true} prefix={'$'} /> {product.free_shipping ? <img src={envio} width="20" height="20" alt="cargando" /> : ''} </p>
                                            <p>{product.title}</p>
                                        </div>
                                    </li>
                                </Link> : ''
                        ))
                    }
                </ul>
            </div>
        </div>
    );
}

export default SearchResult;