import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import NumberFormat from 'react-number-format';

const ProductDetail = () => {

    const { id } = useParams();

    const [product, setProduct] = useState([]);

    React.useEffect(() => {
        const getProductDetail = async () => {
            const data = await fetch(`http://localhost:3001/api/items/${id}`);
            const productData = await data.json();
            setProduct(productData.result);
        }
        getProductDetail()
    }, [id]);




    return (
        <div className="container">
            <div className="col-sm-12">
                {/*Se deja estático el breadcrumb porque en la documentación del test no se especifica con que propiedad tomarlo o armarlo*/} <p style={{ marginTop: '16px', marginBottom: '16px' }}>Telefonos {'>'} Celulares {'>'} Celulares</p>
                <div className="card mt-4">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-sm-8">
                                <img src={product.item && product.item.picture} width="680" alt="cargando" />
                            </div>
                            <div className="col-sm-4">
                                <form>
                                    <p style={{ marginTop: '32px', marginBottom: '16px' }}>{product.item && product.item.condition === "new" ? 'Nuevo' : 'Usado'} - {product.item && product.item.sold_quantity} vendidos</p>
                                    <p><b>{product.item && product.item.title}</b></p>
                                    <p style={{ fontSize: 'x-large', marginTop: '32px', marginBottom: '32px' }}><b><NumberFormat value={product.item && product.item.price.amount} displayType={'text'} thousandSeparator={true} prefix={'$'} /></b></p>
                                    <button style={{ marginTop: '32px', marginRight: '32px', backgroundColor: '#3483FA', color: 'white' }} className="btn btn-block">Comprar</button>
                                </form>
                            </div>
                        </div>
                        <div className="col-sm-8" style={{ marginLeft: '32px' }}>
                            <h2>Descripcion del producto</h2>
                            <p style={{ marginTop: '32px', marginBottom: '32px' }}>{product.item && product.item.description}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ProductDetail;