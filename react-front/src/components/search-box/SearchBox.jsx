import React, { useState } from 'react';
import logo from '../../assets/images/logo.png'
import {
    Link
} from "react-router-dom";

const SearchBox = () => {

    const [searchBox, setSearchBox] = useState({
        searchBox: ''
    });

    const handleInput = (e) => {
        setSearchBox({ searchBox: e.target.value });
    }


    return (
        <React.Fragment>
            <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: "#FFE600" }}>
                <div className="navbar-brand" style={{ paddingLeft: '10.5%' }}>
                    <Link to={`/`}>
                        <img width="50" height="35" src={logo} alt="cargando"/>
                    </Link>
                </div>

                <div className="collapse navbar-collapse" id="navbarSupportedContent" style={{ paddingRight: '10.5%' }}>

                    <div className="input-group w-100">
                        <input type="text" className="form-control" placeholder="Nunca dejes de buscar" onChange={handleInput} aria-describedby="basic-addon2" />
                        <div className="input-group-append">
                            <Link to={`/items/${searchBox.searchBox}`}>
                                <button className="btn btn-outline-secondary" type="button" style={{ backgroundColor: "#EEEEEE" }}><i className="fa fa-search" aria-hidden="true"></i></button>
                            </Link>
                        </div>
                    </div>
                </div>
            </nav>
        </React.Fragment>
    );
}

export default SearchBox;