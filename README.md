-Ejecución-

Para poner en funcionamiento la prueba se deben seguir los siguientes pasos

1. Descargar el proyecto
2. Ingresar por consola a la carpeta /node-back, una vez ubicado en la carpeta se debe ejecutar 'npm install' para instalar las librerias necesarias, una vez intaladas y estando en la misma carpeta por consola se debe ejecutar 'npm run start:dev' para iniciar el servidor y poder acceder al mismo. 
3. Ingresar por consola a la carpeta /react-front y de igual forma ejecutar 'npm install' para instalar las librerias, una vez instaladas y estando en la misma carpeta por consola se debe ejecutar 'npm start' para iniciar la aplicación en el navegador, automaticamente el navegador abre la url 'localhost:3000' sino es asi, puede copiar y pegarla en el navegador.

Realizado por:

Diego Rosero
Development engineer
Contact: (+57) 312 871 9489
Email: lcdiego098@hotmail.com