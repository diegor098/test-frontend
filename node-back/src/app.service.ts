import { HttpService, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class AppService {

  constructor(private http: HttpService) {

  }

  getSearchResult(search: string): Observable<Object> {
    return this.http.get(
      `https://api.mercadolibre.com/sites/MLA/search?q=${search}`
    )
  }

  getProductDetail(id: string): Observable<Object> {
    return this.http.get(
      `https://api.mercadolibre.com/items/${id}`
    )
  }
  getProductDetailDescription(id: string): Observable<Object> {
    return this.http.get(
      `https://api.mercadolibre.com/items/${id}/description`
    )
  }

}
