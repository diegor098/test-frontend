import { Body, Controller, Get, HttpStatus, Req, Res } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  newFormat: any

  constructor(private readonly appService: AppService) { }

  @Get('items')
  async getSearchResult(@Req() req: any, @Res() res) {
    this.appService.getSearchResult(req.query.q).subscribe((response: any) => {
      if (response.data.results.length > 0) {
        this.newFormat = {
          author: {
            name: '',
            lastname: ''
          },
          categories: [],
          items: []
        };
        //Llenando objeto nuevo de acuerdo al formato especificado
        this.newFormat.author.lastname = "ROSERO"
        this.newFormat.author.name = "DIEGO"
        //Obteniendo las categorias
        if(response.data.filters.length > 0){
          response.data.filters[0].values.forEach(e => {
            this.newFormat.categories.push(e.name);
          });
        }
        //Obteniendo los items
        response.data.results.forEach(e => {
          const item = {
            id: e.id,
            title: e.title,
            price: {
              currency: e.installments.currency,
              amount: e.installments.amount
            },
            picture: e.thumbnail,
            condition: e.condition,
            free_shipping: e.shipping.free_shipping
          }
          this.newFormat.items.push(item);
        });
      } else {
        this.newFormat = [];
      }

      return res.json({
        result: this.newFormat
      });
    });
  }
  @Get('items/:id')
  async getProductDetail(@Req() req: any, @Res() res) {
    this.appService.getProductDetail(req.params.id).subscribe((response: any) => {
      this.newFormat = {
        author: {
          name: '',
          lastname: ''
        },
        item: {}
      };
      //Llenando objeto nuevo de acuerdo al formato especificado
      this.newFormat.author.lastname = "ROSERO"
      this.newFormat.author.name = "DIEGO"
      //Obteniendo el item
      this.newFormat.item = {
        id: response.data.id,
        title: response.data.title,
        price: {
          currency: response.data.currency_id,
          amount: response.data.price
        },
        picture: response.data.pictures[0].url,
        condition: response.data.condition,
        free_shipping: response.data.shipping.free_shipping,
        sold_quantity: response.data.sold_quantity,
        description: ""
      }

      this.appService.getProductDetailDescription(response.data.id).subscribe((response: any) => {
        this.newFormat.item.description = response.data.plain_text
        return res.json({
          result: this.newFormat
        });
      });
    });
  }
}
